import Vue from 'vue'
import Loading from 'vue-loading-overlay'

Vue.use(Loading, {
  color: '#9F4F21',
  canCancel: false,
  loader: 'dots'
})
