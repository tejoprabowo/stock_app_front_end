import axios from "axios";

export default async function ({ store, redirect }) {
  const { user } = JSON.parse(window.localStorage.vuex || '{"user":{"login":{"token":"null","isLogin":false}}}')

  if (user.login.isLogin) {
    try {
      // const url = 'https://api.kopikumala.com/public/api/stock/user/checkAuth'
      const url = 'http://localhost:8000/api/stock/user/checkAuth'
      const { data } = await axios.post(url, {}, {
        headers: {
          Authorization: 'Bearer ' + user.login.token
        }
      })

      return await store.dispatch('user/login/setStatusLogin', {
        status: user.login.isLogin
      })
    } catch (e) {
      window.localStorage.clear()
    }
  } else {
    return false
  }
}
