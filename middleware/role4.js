import axios from "axios";

export default async function ({ store, redirect }) {
  const { user } = JSON.parse(window.localStorage.vuex || '{"user":{"login":{"token":"null","isLogin":false}}}')
  try {
    // const url = 'https://api.kopikumala.com/public/api/stock/user/getInfo'
    const url = 'http://localhost:8000/api/stock/user/getInfo'
    const { data } = await axios.post(url, {}, {
      headers: {
        Authorization: 'Bearer ' + user.login.token
      }
    })
    if (data.role_id !== '4') {
      alert('Anda tidak memiliki akses kehalaman yang di tuju!')
      return redirect('/dashboard')
    }
  } catch (e) {
    console.log(e)
  }
}
