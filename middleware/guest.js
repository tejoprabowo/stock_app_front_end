export default async function ({ store, redirect }) {
  if (store.getters['user/login/statusLogin']) {
    return redirect('/dashboard')
  }
}
