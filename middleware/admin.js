export default async function ({ store, redirect }) {
  const { user } = JSON.parse(window.localStorage.vuex || '{"user":{"login":{"token":"null","isLogin":false}}}')
  if (!user.login.isLogin) {
    redirect('/')
  }
}
