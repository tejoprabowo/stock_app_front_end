export const state = () => ({
  token: null,
  isLogin: false
})

export const actions = {
  async loginProcess({ commit }, { user, password }) {
    try {
      const { data } = await this.$axios.post('user/login', {
        user, password
      })
      commit('SAVE_TOKEN_USER', {
        token: data.token
      })
      commit('SET_LOGIN_STATUS_USER', {
        status: true
      })
      this.$router.push('/dashboard')
      const dataRes = {
        err: false,
        statusLogin: true
      }
      return dataRes
    } catch (e) {
      const dataRes = {
        err: e,
        statusLogin: false
      }
      return dataRes
    }
  },

  setTokenUser({ commit }, { token }) {
    commit('SAVE_TOKEN_USER', {
      token: token
    })
  },

  setStatusLogin({ commit }, { status }) {
    commit('SET_LOGIN_STATUS_USER', {
      status: status
    })
  }
}

export const mutations = {
  SAVE_TOKEN_USER(state, { token }) {
    state.token = token
  },

  SET_LOGIN_STATUS_USER(state, { status }) {
    state.isLogin = status
  }
}

export const getters = {
  tokenPure(state) {
    return state.token
  },

  statusLogin(state) {
    return state.isLogin
  }
}
